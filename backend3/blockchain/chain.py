import hashlib
import datetime
import json
import requests
import traceback

peer_nodes = [
	#'http://localhost:9000/',
	'http://localhost:8800/',
	'http://localhost:8000/',
]

class Blockchain(object):
	instance = None
	def __init__(self):
		self.chain = []
		self.create_block(1, '0', {'key': "genesis"})

	def create_block(self, proof, previous_hash, data):
		block = {
			'index': len(self.chain) + 1,
			'timestamp': str(datetime.datetime.now()),
			'data':data,
			'proof': proof,
			'previous_hash': previous_hash
		}

		self.chain.append(block)
		#self.broadcast(block)

		return block
		
		
	def broadcast(self, block=None):
		if block:
			block = json.dumps(block)
			for node_url in peer_nodes:
				try:
					response = requests.post(node_url + "blockchain/add_block/", data=block)
				except:
					pass


	def add_block(self,block=None):
		if block:
			self.chain = block

	def get_previous_block(self):
		if len(self.chain) == 0:
			self.create_block(1, '0', {'key': "genesis"})
		return self.chain[-1]


	def hash(self, block):
		encoded_block = json.dumps(block, sort_keys = True).encode()
		return hashlib.sha256(encoded_block).hexdigest()
		

	def proof_of_work(self, previous_proof):
		new_proof = 1
		check_proof = False
		while check_proof is False:
			hash_operation = hashlib.sha256(str(new_proof**2 - previous_proof**2).encode()).hexdigest()
			if hash_operation[:4] == "0000":
				check_proof = True
			else:
				new_proof += 1
		return new_proof


	def is_chain_valid(self, chain):
		previous_block = chain[0]
		block_index = 1
		while block_index < len(chain):
			block = chain[block_index]
			if block['previous_hash'] != self.hash(previous_block):
				return False
			previous_block = block
			block_index += 1

		return True
		
	def find_new_chains(self):
		other_chains = []
		for node_url in peer_nodes:
			try:
				chain = requests.get(node_url + "blockchain/get_chain/").content
				print(chain)
				chain = json.loads(chain.decode('utf-8'))
				other_chains.append(chain)
			except:
				traceback.print_exc()
		return other_chains


	def consensus(self):
		other_chains = self.find_new_chains()
		longest_chain = self.chain

		for chain in other_chains:
			if len(longest_chain) < len(chain):
				longest_chain = chain

		self.chain = longest_chain



	@classmethod
	def get_instance(kclass):
		if kclass.instance is None:
			kclass.instance = Blockchain()
		return kclass.instance
