from PIL import Image
import base64
import json
import os
import traceback

from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response 
from rest_framework import status
from django.conf import settings
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User 
from django.views.decorators.csrf import csrf_exempt


from .chain import Blockchain

from authentication.models import Student
from authentication.forms import *
from icardmanagement.models import IDCard

@api_view(['GET', 'POST'])
def create_id_card(request):
	if request.method=="POST":
		data = dict(request.data)
		imagepath = settings.BASE_DIR + "/static/dummy.jpg"
		student, created = Student.objects.get_or_create(**data)
		with open(imagepath, 'rb') as img:
			img_base64 = base64.b64encode(img.read())
		data['image'] = img_base64.decode('utf-8')
		
		blockchain = Blockchain.get_instance()
		prev_block = blockchain.get_previous_block()
		prev_proof = prev_block['proof']
		prev_hash = blockchain.hash(prev_block)
		proof = blockchain.proof_of_work(prev_proof)

		block = blockchain.create_block(proof, prev_hash, data)
		try:
			id_card= IDCard.objects.get(student=student)
			id_card.block_index = block['index']
			id_card.save()
		except:
			id_card = IDCard.objects.create(student=student, block_index=block['index'])
		
	return Response({'block': block, 'data': data},status=200)


@login_required()
def create_idcard(request):
	messages = []
	if request.method == "POST":
		try:
			print(request.FILES)
			password = request.POST.get('password')
			userForm = UserForm(request.POST)
			studentForm = CreateStudentForm(request.POST, request.FILES)
			if userForm.is_valid() and studentForm.is_valid():
				user = userForm.save(commit=False)
				user.set_password(password)
				user.save()

				student = studentForm.save(commit=False)
				student.user = user
				student.save()
				imagepath = settings.BASE_DIR + student.image.url
				with open(imagepath, 'rb') as img:
					img_base64 = base64.b64encode(img.read())

				data = {
					'fullname': student.fullname,
					'registration_number': student.registration_number,
					'attached_hall': student.attached_hall,
					"department": student.department_institute,
					'session': student.session,
					'contact_number': student.user.username,
					'residential_status': student.residential_status,
					'image': img_base64.decode('utf-8')
				}

				blockchain = Blockchain.get_instance()
				prev_block = blockchain.get_previous_block()
				prev_proof = prev_block['proof']
				prev_hash = blockchain.hash(prev_block)
				proof = blockchain.proof_of_work(prev_proof)

				block = blockchain.create_block(proof, prev_hash, data)
				try:
					id_card= IDCard.objects.get(student=student)
					id_card.block_index = block['index']
					id_card.save()
				except:
					id_card = IDCard.objects.create(student=student, block_index=block['index'])
				print(block['index'])

				messages.append("Successfully created student at block index: %d" % block['index'])
			else:
				messages.append("Form Processing Failed")

			
		except:
			traceback.print_exc()
			messages.append("Incorrect Data format")
			
	return render(request, 'authentication/add_student.html',  {'messages': messages })


def blocks(request):
	blockchain = Blockchain.get_instance()
	return render(request, 'blockchain/blocks.html', {'chain': blockchain.chain})


@csrf_exempt
def add_block(request):
	if request.method=="POST":
		block = json.loads(request.body.decode('utf-8'))
		
		blockchain = Blockchain.get_instance()
		blockchain.add_block(block)
		return JsonResponse({},status=200)
		


@api_view(["GET",])
def get_chain(request):
	blockchain = Blockchain.get_instance()
	chain = blockchain.chain
	length = len(chain)
	return Response({'chain': chain, 'length': length})


@api_view(["GET"])
def chain_valid(request):
	blockchain = Blockchain.get_instance()
	is_valid = blockchain.is_chain_valid(blockchain.chain)
	msg = ""
	if is_valid:
		msg = "Congratulations! Chain is valid"
	else:
		msg = "Chain is compromised"

	return Response({'message': msg})


@csrf_exempt
def get_id_card_info(request, blockindex):
	blockchain = Blockchain.get_instance();
	chain = blockchain.chain

	block = None

	for i in range(len(chain)):
		if(chain[i]['index']==blockindex):
			block = chain[i]

	if block is None:
		return JsonResponse({}, status=404)

	return JsonResponse(block['data'], status=200)



