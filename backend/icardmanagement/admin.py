from django.contrib import admin

# Register your models here.
from .models import IDCard

admin.site.register(IDCard)