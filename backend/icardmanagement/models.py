from django.db import models

from authentication.models import Student
# Create your models here.
class IDCard(models.Model):
	student = models.OneToOneField(Student, on_delete=models.CASCADE)
	block_index = models.IntegerField()
