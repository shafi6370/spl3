from django.urls import path

from . import views

urlpatterns = [
	path('create_id_card/', views.create_id_card, name="create_id_card"),
	path('create_idcard/', views.create_idcard, name='create_idcard'),
	path('get_chain/', views.get_chain, name="get_chain"),
	path('chain_valid/', views.chain_valid, name="chain_valid"),
	path('add_block/', views.add_block, name="add_block"),
	path('blocks/', views.blocks, name='blocks'),
	path('id_info/<int:blockindex>/', views.get_id_card_info, name="get_id_card_info"),
]