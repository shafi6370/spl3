from django.db import models
from django.contrib.auth.models import User

class Administrator(models.Model):
	fullname = models.CharField(max_length=50)
	user = models.OneToOneField(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.fullname


class Official(models.Model):
	fullname = models.CharField(max_length=50)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	designation = models.CharField(max_length=50)
	attached_office = models.CharField(max_length=50)

	def __str__(self):
		return self.fullname


RESIDENTIAL_STATUS_CHOICES = [
	(1, "Residential"),
	(2, "Non-Residential")
]

class Student(models.Model):
	fullname = models.CharField(max_length=50)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	father_name = models.CharField(max_length=50)
	mother_name = models.CharField(max_length=50)
	registration_number = models.CharField(max_length=12)
	attached_hall = models.CharField(max_length=50)
	department_institute = models.CharField(max_length=50)
	session = models.CharField(max_length=10)
	image = models.ImageField(upload_to="student", null=True)
	residential_status = models.IntegerField(choices=RESIDENTIAL_STATUS_CHOICES, default=2)

	def __str__(self):
		return self.fullname

