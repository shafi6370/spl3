from django.apps import AppConfig


class IcardmanagementConfig(AppConfig):
    name = 'icardmanagement'
