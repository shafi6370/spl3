from django.urls import path

from . import views

urlpatterns = [
	path('login/', views.login_view, name="login"),
	path('api_login/', views.api_login, name="api_login"),
	path('api_login_student/', views.api_login_student, name='api_login_student'),
	path('logout/', views.logout_view, name="logout"),
	path('students/add/', views.add_student, name='add_student'),
	path('students/', views.students, name="students"),
	path('staffs/add/', views.add_staff, name='add_staff'),
	path('staffs/', views.staffs, name='staffs')
]