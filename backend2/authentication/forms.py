from django import forms

from authentication.models import *
from django.contrib.auth.models import User 


class CreateStudentForm(forms.ModelForm):
	class Meta:
		model = Student
		exclude = ['user',]


class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username',]


class StaffForm(forms.ModelForm):
	class Meta:
		model = Official
		exclude = ['user',]