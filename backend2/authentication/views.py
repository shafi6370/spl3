import traceback
import json
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.response import Response 
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from .forms import *
from .models import *

# Create your views here.
def login_view(request):
	error = {}
	if request.user.is_authenticated:
		return redirect("/blockchain/blocks/")
	elif request.method=="POST":
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			return redirect("/blockchain/blocks/")
		error = {'error': "Invalid credentials"}

	return render(request, 'authentication/login.html', error)


def logout_view(request):
	logout(request)
	return render(request, 'authentication/login.html')





@csrf_exempt
def api_login(request):
	data = json.loads(request.body.decode('utf-8'))
	username = data.get('username')
	password = data.get('password')
	print(username, password)
	
	try:
		user = User.objects.get(username=username)
		print("user", user)
		if user.check_password(password):
			print("chk pass")
			return JsonResponse({'auth': True}, status=202)
		else:
			print("chk pass failed")
	except User.DoesNotExist:
		print("no user")
	return JsonResponse({'auth': False}, status=401)



@csrf_exempt
def api_login_student(request):
	data = json.loads(request.body.decode('utf-8'))
	username = data.get('username')
	password = data.get('password')
	print(username, password)
	
	try:
		user = User.objects.get(username=username)
		print("user", user)
		if user.check_password(password):
			print("chk pass")
			if hasattr(user, "student"):
				student = user.student
				return JsonResponse({'auth': True, 'blockindex': student.idcard.block_index}, status=202)
		else:
			print("chk pass failed")
	except User.DoesNotExist:
		print("no user")
	return JsonResponse({'auth': False}, status=401)




# @csrf_exempt
# def api_login(request):
# 	if request.method == "POST":	
# 		try:
# 			username = request.POST.get('username')
# 			password = request.POST.get('password')
# 			print(username, password)
			
# 			try:
# 				user = User.objects.get(username=username)
# 				if user.check_password(password):
# 					return JsonResponse({'auth': True}, status=202)
# 			except User.DoesNotExist:
# 				pass
# 			print("mama")
# 			return JsonResponse({'auth': False}, status=401)
# 		except:

# 			return JsonResponse({'auth': False}, status=401)
# 	return JsonResponse({'auth': False}, status=401)
		

@login_required
def students(request):
	students = Student.objects.all()
	return render(request, 'authentication/students.html',  {'students': students})

@login_required
def staffs(request):
	staffs = Official.objects.all()
	return render(request, 'authentication/staffs.html', {'staffs': staffs})

@login_required
def add_student(request):
	return render(request, 'authentication/add_student.html');

@login_required
def add_staff(request):
	messages = []
	if request.method == "POST":
		try:	
			password = request.POST.get('password')
			userform = UserForm(request.POST)
			staffform = StaffForm(request.POST)

			if userform.is_valid() and staffform.is_valid():
				user = userform.save(commit=False)
				user.set_password(password)
				user.save()
				official = staffform.save(commit=False)
				official.user = user
				official.save()

				messages.append("Succesfully created official " + official.fullname)
			else:
				messages.append("Cannot process form")
		except:
			traceback.print_exc()
			messages.append("Incorrect data format")

	return render(request, 'authentication/add_staff.html', {'messages': messages});