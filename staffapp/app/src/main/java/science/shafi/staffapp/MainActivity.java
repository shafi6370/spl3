package science.shafi.staffapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity {

    Button loginButton;
    Button resetButton;
    EditText userNameEditText;
    EditText passwordEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userNameEditText = (EditText) findViewById(R.id.username);
        passwordEditText = (EditText) findViewById(R.id.password);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = userNameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                User user = User.getInstance(username, password, getApplicationContext());

                try {
                   user.login();
                   Log.i("MAIN", "Trying Login Logged in " + user.isLoggedIn());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(user.isLoggedIn()) {
                    Intent intent = new Intent(v.getContext(), ScanQRCodeActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userNameEditText.setText("");
                passwordEditText.setText("");
            }
        });
    }
}
