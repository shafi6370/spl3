package science.shafi.staffapp;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shafi on 11/25/18.
 */

public class User {
    private static User user;

    private String username;
    private String password;
    private Context context;

    private static boolean loggedIn;

    private User(String username, String password, Context context){
        this.username = username;
        this.password = password;
        this.context = context;
        loggedIn = false;
    }

    public static User getInstance(String username, String password, Context context){
        if(user==null){
         user = new User(username, password, context);
        }
        else{
            user.username = username;
            user.password = password;
        }
        return user;
    }


    public void
    login() throws JSONException {
        String url = Consts.host + "auth/api_login/";
        JSONObject postParams = new JSONObject();
        postParams.put("username", username);
        postParams.put("password", password);
        Log.i("MAIN", "Login called");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("MAIN", "Response " + response.has("auth"));
                try {
                    loggedIn = response.getBoolean("auth");
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

        WebReqHandler.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public boolean isLoggedIn(){
        return loggedIn;
    }
}
