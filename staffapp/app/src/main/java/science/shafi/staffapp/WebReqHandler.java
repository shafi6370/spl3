package science.shafi.staffapp;
import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by shafi on 6/10/18.
 */

public class WebReqHandler {

    private static WebReqHandler mWebReqHandler;
    private static Context mCtx;
    private RequestQueue mRequestQueue;

    private WebReqHandler(Context context){
        mCtx = context;
        mRequestQueue = getmRequestQueue();
    }


    public static synchronized WebReqHandler getInstance(Context context){
        if(mWebReqHandler == null){
            mWebReqHandler = new WebReqHandler(context);
        }
        return mWebReqHandler;
    }


    private RequestQueue getmRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req){
        getmRequestQueue().add(req);
    }
}
