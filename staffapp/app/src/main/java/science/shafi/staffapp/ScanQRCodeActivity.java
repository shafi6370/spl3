package science.shafi.staffapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.vision.barcode.BarcodeDetector;


public class ScanQRCodeActivity extends AppCompatActivity {
    Button qrBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);
        qrBtn = (Button) findViewById(R.id.qr_codeButton);



        qrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(v.getContext(), QRSCanner.class);
                startActivity(intent);
            }
        });
    }
}
