package science.shafi.studentapp;

/**
 * Created by shafi on 11/24/18.
 */

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class WebReqHandler {
    private static WebReqHandler mWebReqHandler;
    private static Context mCtx;
    private static RequestQueue mRequestQueue;

    private WebReqHandler(Context ctx){
        mCtx = ctx;
        mRequestQueue = getmRequestQueue();
    }

    public static synchronized WebReqHandler getInstance(Context ctx){
        if(mWebReqHandler==null){
            return new WebReqHandler(ctx);
        }
        return mWebReqHandler;
    }

    private RequestQueue getmRequestQueue(){
        if(mRequestQueue==null){
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request){
        getmRequestQueue().add(request);
    }
}