package science.shafi.studentapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class IDCardActivity extends AppCompatActivity {

    Button qr_code;
    private TextView studentName;
    private TextView regNo;
    private TextView hall;
    private TextView department;
    private TextView rs_stat;
    private TextView contactNo;
    private ImageView studentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idcard);
        studentName = (TextView) findViewById(R.id.studentNameLabel);
        regNo = (TextView) findViewById(R.id.regNo);
        hall = (TextView) findViewById(R.id.hall);
        department = (TextView) findViewById(R.id.department);
        rs_stat = (TextView) findViewById(R.id.rs_stat);
        contactNo = (TextView) findViewById(R.id.contact_no);
        studentImage = (ImageView) findViewById(R.id.studentImage);
        final int blockindex = getIntent().getIntExtra("blockindex", 0);

        qr_code = (Button) findViewById(R.id.qr_btn);

        qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), QRCodeActivity.class);
                String url = Consts.host + "blockchain/id_info/" + blockindex + "/";
                intent.putExtra("URL", url);
                startActivity(intent);
            }
        });

        loadIDCard(blockindex);
    }

    private void loadIDCard(int blockindex){
        String url = Consts.host + "blockchain/id_info/" + blockindex + "/";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    studentName.setText(response.getString("fullname"));
                    regNo.setText(response.getString("registration_number"));
                    hall.setText(response.getString("attached_hall"));
                    department.setText(response.getString("department"));
                    contactNo.setText(response.getString("contact_number"));
                    int rs = response.getInt("residential_status");
                    if(rs==1){
                        rs_stat.setText("Residential");
                    }else{
                        rs_stat.setText("Non-Residential");
                    }

                    byte[] decodedString = Base64.decode(response.getString("image"), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    studentImage.setImageBitmap(Bitmap.createScaledBitmap(decodedByte, studentImage.getWidth(), studentImage.getHeight(), false));

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("MAIN", error.toString());
                Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_SHORT).show();
            }
        });

        WebReqHandler.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }
}
